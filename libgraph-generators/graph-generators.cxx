#include <algorithm>
#include <libgraph-generators/graph-generators.hxx>
#include <queue>
#include <random>
#include <unordered_set>
#include <vector>

namespace graph_generators
{
struct edge_hash
{
public:
    template<typename T, typename U>
    auto operator()(const std::pair<T, U>& x) const -> std::size_t
    {
        return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
    }
};

static auto generator() -> std::mt19937&;
static auto next_probability() -> float;
static auto fat(uint32_t n) -> uint32_t;
static auto comb(uint32_t n, uint32_t k) -> uint32_t;

static auto degrees_from_dist(
    uint32_t n, std::discrete_distribution<uint32_t>* distribution,
    const std::vector<uint32_t>& degree_labels) -> std::vector<uint32_t>;

auto make_gnm(uint32_t n, uint32_t m) -> edge_stream
{
    std::vector<edge> edges;
    edges.reserve(n * n / 2
                  - n); // n*n/2 = undirected, -n = remove self-loop diagonal

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            edges.emplace_back(u, v);
        }
    }

    std::shuffle(edges.begin(), edges.end(), generator());

    for(uint32_t i = 0; i < m; ++i)
    {
        co_yield edges[i];
    }
}

auto make_gnp(uint32_t n, float p) -> edge_stream
{
    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            if(next_probability() < p)
            {
                co_yield {u, v};
            }
        }
    }
}

auto make_gnpc(uint32_t n, float c) -> edge_stream
{
    return make_gnp(n, c / static_cast<float>(n - 1));
}

auto make_gnpm(uint32_t n, uint32_t m) -> edge_stream
{
    return make_gnp(n, static_cast<float>(m) / static_cast<float>(comb(n, 2)));
}

auto make_gnps(uint32_t n, float s) -> edge_stream
{
    return make_gnpc(n, -std::log(1.F - s) / s);
}

auto make_gnm_config(const std::vector<uint32_t>& degrees) -> edge_stream
{
    uint32_t total_stubs = std::accumulate(degrees.begin(), degrees.end(), 0U);
    uint32_t m = total_stubs / 2;

    std::vector<uint32_t> stubs = degrees; // will need to modify it
    std::vector<uint32_t> indices(stubs.size());

    std::iota(indices.begin(), indices.end(), 0);

    while(m > 0)
    {
        std::shuffle(indices.begin(), indices.end(), generator());
        int32_t i = -1;

        while(stubs[indices[++i]] == 0)
        {
        }
        uint32_t u = indices[i];

        uint32_t ui = i;
        while(stubs[indices[++i]] == 0 && ui < indices.size())
        {
        }

        if(ui == indices.size())
        {
            break;
        }

        uint32_t v = indices[ui];

        co_yield {u, v};

        --m;
        --stubs[u];
        --stubs[v];
    }
}

auto make_gnm_config(uint32_t n,
                     std::discrete_distribution<uint32_t>* distribution,
                     std::vector<uint32_t> degree_labels) -> edge_stream
{
    if(degree_labels.empty())
    {
        for(uint32_t i = 0; i < distribution->max() + 1; ++i)
        {
            degree_labels.push_back(i);
        }
    }

    std::vector<uint32_t> degrees =
        degrees_from_dist(n, distribution, degree_labels);

    return make_gnm_config(degrees);
}

auto make_gnp_config(const std::vector<uint32_t>& degrees) -> edge_stream
{
    uint32_t n = degrees.size();
    uint32_t m = std::accumulate(degrees.begin(), degrees.end(), 0U) / 2;

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            if(next_probability() < static_cast<float>(degrees[u] * degrees[v])
                                        / static_cast<float>(2U * m))
            {
                co_yield {u, v};
            }
        }
    }

    for(uint32_t u = 0; u < n; ++u)
    {
        if(next_probability() < degrees[u] / static_cast<float>(4U * m))
        {
            co_yield {u, u};
        }
    }
}

auto make_gnp_config(uint32_t n,
                     std::discrete_distribution<uint32_t>* distribution,
                     std::vector<uint32_t> degree_labels) -> edge_stream
{
    if(degree_labels.empty())
    {
        for(uint32_t i = 0; i < distribution->max() + 1; ++i)
        {
            degree_labels.push_back(i);
        }
    }

    std::vector<uint32_t> degrees =
        degrees_from_dist(n, distribution, degree_labels);

    return make_gnp_config(degrees);
}

auto make_price(uint32_t n, uint32_t c, uint32_t a) -> edge_stream
{
    std::vector<uint32_t> in_degrees(n * c);
    float threshold = c / static_cast<float>(c + a);

    uint32_t size = 0;
    for(; size < c; ++size)
    {
        in_degrees[size] = size;
    }

    for(uint32_t u = c; u < n; ++u)
    {
        // to check if new random edges were not yielded before
        std::unordered_set<edge, edge_hash> edges;

        // every node has out-degree exactly c, no fluctuation
        while(edges.size() < c)
        {
            uint32_t v = next_probability() < threshold
                             ? in_degrees[next_probability() * size]
                             : next_probability() * static_cast<float>(u - 1);

            edge e = {u, v};
            if(edges.find(e) == edges.end())
            {
                co_yield e;
                edges.insert(e);
                in_degrees[size++] = v;
            }
        }
    }
}

auto make_small_world(uint32_t n, uint32_t c, float p) -> edge_stream
{
    uint32_t halfc = c / 2;

    std::vector<std::unordered_set<uint32_t>> g(n);

    for(uint32_t i = 0; i < n; ++i)
    {
        int32_t a = i - halfc;
        int32_t b = n;

        uint32_t lower = ((a % b) + b) % b;
        uint32_t upper = (i + halfc + 1) % n;

        for(uint32_t j = lower; j != upper; j = (j + 1) % n)
        {
            if(i != j)
            {
                g[i].insert(j);
            }
        }
    }

    for(uint32_t u = 0; u < n; ++u)
    {
        for(auto v: g[u])
        {
            if(u < v && next_probability() < p)
            {
                g[u].erase(v);
                g[next_probability() * n].insert(next_probability() * n);
            }
        }
    }

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v: g[u])
        {
            co_yield {u, v};
        }
    }
}

auto make_single_source(uint32_t n) -> edge_stream
{
    for(uint32_t v = 1; v < n; ++v)
    {
        co_yield { 0, v };
    }
}

auto make_full_tree(uint32_t arity, uint32_t depth) -> edge_stream
{
    float farity = arity;
    float a = std::pow(farity, static_cast<float>(depth));
    float num = std::ceil(a) - 1;
    float den = farity - 1;
    uint32_t n = num / den;

    uint32_t i = 0;
    std::queue<uint32_t> q;
    q.push(i++);

    while(i < n - 1) // every node but root has an incident edge
    {
        uint32_t u = q.front();
        q.pop();
        for(uint32_t j = 0; j < arity; ++j)
        {
            co_yield {u, ++i};
            q.push(i);
        }
    }
}

static auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r(), r(), r(), r(), r(), r()};
    static std::mt19937 gen(seed);

    return gen;
}

static auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

static auto fat(uint32_t n) -> uint32_t
{
    uint32_t acc = 1;
    for(uint32_t factor = 2; factor <= n; ++factor)
    {
        acc *= factor;
    }

    return acc;
}

static auto comb(uint32_t n, uint32_t k) -> uint32_t
{
    return fat(n) / (fat(k) * fat(n - k));
}

static auto degrees_from_dist(
    uint32_t n, std::discrete_distribution<uint32_t>* distribution,
    const std::vector<uint32_t>& degree_labels) -> std::vector<uint32_t>
{
    std::vector<uint32_t> degrees(n);

    for(uint32_t& degree: degrees)
    {
        degree = degree_labels[distribution->operator()(generator())];
    }

    return degrees;
}

} // namespace graph_generators
