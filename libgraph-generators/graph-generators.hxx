#pragma once

#include <cppcoro/generator.hpp>
#include <libgraph-generators/export.hxx>
#include <random>

namespace graph_generators
{
using edge = std::pair<uint32_t, uint32_t>;
using edge_stream = cppcoro::generator<edge>;

// clang-format off

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_gnm(uint32_t n, uint32_t m) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_gnp(uint32_t n, float p) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT // c = mean degree
auto make_gnpc(uint32_t n, float c) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT // m = mean edges
auto make_gnpm(uint32_t n, uint32_t m) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT // s = mean biggest component's fraction
auto make_gnps(uint32_t n, float s) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_gnm_config(const std::vector<uint32_t>& degrees) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_gnm_config(uint32_t n,
                     const std::discrete_distribution<uint32_t>& distribution,
                     const std::vector<uint32_t>& degree_labels) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_gnp_config(const std::vector<uint32_t>& degrees) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_gnp_config(uint32_t n,
                     const std::discrete_distribution<uint32_t>& distribution,
                     const std::vector<uint32_t>& degree_labels) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_price(uint32_t n, uint32_t c, uint32_t a = 1) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_small_world(uint32_t n, uint32_t c, float p) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_single_source(uint32_t n) -> edge_stream;

LIBGRAPH_GENERATORS_SYMEXPORT
auto make_full_tree(uint32_t arity, uint32_t depth) -> edge_stream;

// clang-format on

} // namespace graph_generators
